<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateLectures extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lectures = [
            ['Introduction', "Introducing students to Meta-Programming's vision and Python Programming", 'lectures/programming/Lecture 1.pdf'],
            ['Python Basics', "Here we'll introduce students to the fundamentals of the Python Programming Language", 'lectures/programming/Lecture 2.pdf'],
            ['Types of Thinking & Python Data Structures', "We go through the different types of Thinking. After which we go through Python's Data Structures", 'lectures/programming/Lecture 3.pdf'],
            ['Attitues of Learning & Python Data Structures Continued', "We discuss the approaches to learning and doing great work. We then complete our discussion on Python Data Structures", 'lectures/programming/Lecture 4.pdf'],
            ['File Input & Output', "In this lecture we break down the modes used for accessing and manipulating files with Python", 'lectures/programming/Lecture 5.pdf'],
            ['User Defined Functions', "We conclude the program by showing you how you can define your own functions and how you can reference them", 'lectures/programming/Lecture 6.pdf'],           
        ];

        foreach ($lectures as $lecture) {
            DB::table('programming')->insert([
                'lecture_name' => $lecture['0'],
                'lecture_info' => $lecture['1'],
                'lecture_slide_path' => $lecture['2'],
            ]);
        }
    }
}
