<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            //['Abdulkabir', 'Adegoke','akbadegoke@yahoo.co.uk',''],
            // ['Michael','Odeh','odehmichael@hotmail.com','2348138330458','student','2','Ya^6PFZq'],
            // ['Oluwaferanmi','Olufunminiyi','funminiyideborah@gmail.com','2347058155397','student','2','H%z8&Jb$']
            ['Solomon', 'Lawuyi', 'dlawuyi@gmail.com', '2348037380028', 'student', '2', 'H%z8&Jb&' ],
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                'fname' => $user[0],
                'lname' => $user[1],
                'email' => $user[2],
                'phone' => $user[3],
                'account_type' => $user[4],
                'cohort_id' => $user[5],
                'password' => Hash::make($user[6]),
            ]);
        }

    }
}
