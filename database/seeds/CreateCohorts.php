<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CreateCohorts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cohorts = [
            ['cohort_one','programming','2020-09-12 10:00:00','2020-11-06 13:00:00'],
            ['cohort_two','programming','2021-01-09 10:00:00',NULL],
        ];

        foreach($cohorts as $cohort) {
            DB::table('cohorts')->insert([
                'cohort_name' => $cohort[0],
                'class_type' => $cohort[1],
                'began' => $cohort[2],
                'ended' => $cohort[3],
            ]);
        }
    }
}
