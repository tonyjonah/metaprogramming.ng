<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CohortUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cohort_users', function (Blueprint $table) {
            $table->unsignedBigInteger('cohort_id');
            $table->unsignedBigInteger('user_id');

            $table->unique(['cohort_id','user_id']);
        });

        Schema::enableForeignKeyConstraints();

        Schema::table('cohort_users', function (Blueprint $table) {
            $table->foreign('cohort_id')->references('id')->on('cohorts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTableIfExists('cohort_users');
    }
}
