<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisteredWardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_wards', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('registrar_id');
            $table->index('registrar_id');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('fname');
            $table->string('lname');
            $table->date('dob');
            $table->timestamp('create_at');
        });

        Schema::enableForeignKeyConstraints();

        Schema::table('registered_wards', function (Blueprint $table) {
            $table->foreign('registrar_id')->references('id')->on('registrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_wards');
    }
}
