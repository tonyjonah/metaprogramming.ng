<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/blog/entry/compose', 'BlogController@create');
Route::post('/blog/entry/compose', 'BlogController@store');
Route::get('/blog/entries','BlogController@index');
Route::get('/blog/entry/show/{article}', 'BlogController@show');

Route::get('/', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/blog', function () {
//     return view('blog');
// });

Route::get('/faq', function () {
    setlocale(LC_MONETARY, 'en_NG');
    return view('faq');
});

Route::get('/coming', function () {
    return view('coming-soon');
});

Route::get('/confirmation', function () {
    return view('confirmation');
});

Route::get('/showcase', function () {
    return view('showcase');
});

// Route::get('/registration', 'RegistrationController@show');

Route::post('/pay', [
    'uses' => 'PaymentsController@redirectToGateway',
    'as' => 'pay'
]);
Route::get('/payment/callback', 'PaymentsController@handleGatewayCallback');
Route::get('/payment/event', 'PaymentsController@handleGatewayEventNotification');

Route::post('/email', 'AddEmailController@store');
Route::post('/contact', 'ContactUsController@store');
Route::get('/contact', function () {
    return view('contact');
});
Route::post('/contact/address', 'ContactUsController@saveAddress');

Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/freetechtraining', '\App\Http\Controllers\Auth\RegisterController@registerFreeTraning')->name('freetraining');
Route::get('/freetechtraining', '\App\Http\Controllers\Auth\RegisterController@showFreeTraningForm');
Route::get('/student/dashboard', 'StudentController@showDashboard');
Route::get('/student/information', 'StudentController@showSessionInfo');
Route::get('/parentSponsor/dashboard', 'ParentSponsor@showDashboard');
Route::get('/cohort/{id}/session/lectures', 'StudentController@showProgrammingLectures');

// Route::get('/cohort/{id}/session/{num}', 'StudentController@showSessionInfo');

// Route::get('/cohort/{id}/session/{num}/assignment', 'StudentController@viewAssignment');
// Route::post('/cohort/{id}/session/{num}/assignment', 'StudentController@submitAssignment');

//Route::get('test_mail', function() {
//      return new App\Mail\SendCredentials();
//});

// Route::get('videolink', function() {
//     return new App\Mail\SendLastWeeksVideo();
// });