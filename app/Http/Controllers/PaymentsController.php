<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Paystack;
use Validator;
use App\Payments;
use App\Registration;
use App\RegisteredWards;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Events\PaidAccountCreationEvent;
use App\Http\Controllers\RegistrationController;
use App\Notifications\ConfirmRegisterationPayment;

class PaymentsController extends Controller
{
    /**
     * Data for ParentsData
     */
    public $parentsData;

    /**
     * Data for Students
     */
    public $studentsData;

    /**
     * Data for Wards
     */
    public $wardsData;

    /**
     * redirectToGateway()
     * 
     * Redirect the registrant to paystack
     */
    public function redirectToGateway(Request $request)
    {
        $rules = [
            "fname" => "required|string",
            "lname" => "required|string",
            "email" => "required|email",
            "phone" => "required|string",
            "num"   => "nullable|numeric",
            "selfRegistration" => "required|numeric",
            "ward_fname"    => "nullable|array|min:1",
            "ward_fname*"  => "string|distinct|min:1",
            "ward_lname" => "array|min:1",
            "ward_lname*"  => "string|min:1",
            "ward_email" => "array",
            "ward_email*" => "email|unique",
            "ward_phone" => "array",
            "ward_phone*" => "string",
            "dob" => "nullable|array",
        ];

        $messages = [
            'fname.required' => 'Your First Name is Required',
            'lname.required' => 'Your Last Name is Required',
            'email.required' => 'Your email Address is Required',
            'phone.required' => 'Your Phone Number is Required',
            'num.required' => 'The Number of Wards / Children Being Registered is Required',
            'ward_fname.*' => [
                'required' => "The Ward / Child's First Name is Required",
                'min' => "You need to enter the name of The First Name of Your Ward / Child"
            ],
            'ward_lname.*.required' => "The Ward / Child's Last Name is Required",
            'dob.*.required' => "The Ward / Child's Date of Birth is Required"
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // dd($request->amount);

        $registration = new Registration;

        try {
            DB::beginTransaction();

            // generate random 8 char password from below chars
            // $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');

            if($request->selfRegistration == 0) {
                $registration->email = $request->email;
                $registration->phone = $request->phone;
                $registration->fname = $request->fname;
                $registration->lname = $request->lname;
                $registration->num_of_wards_registered = $request->num;
                $registration->amount_payable = $request->amount;
                $registration->payment_confirmation = 0;
                $registration->reference = $request->reference;
                $registration->created_at = now();
        
                $registration->save();

                // Confirm if parent has an existing account
                if(User::where('email', $registration->email)->first() != null) {
                    Log::info('User with ' . $registration->email . ' already exists');
                }

                // Assign Parents Data
                
                $this->parentsData = [
                    'fname' => $registration->fname,
                    'lname' => $registration->lname,
                    'email' => $registration->email,
                    'phone' => $registration->phone,
                    'account_type' => 'parent_or_sponsor',
                    'password' => substr(str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&'), 0, 8)
                ];

                $this->studentsData = null;

                if( $request->input('ward_email.1') != null ) {
                    $wards = new RegisteredWards;
        
                    for($i = 1; $i <= $request->num; $i++) {
                        $wards->registrar_id = $registration->id;
                        $wards->email = $request->input('ward_email.' . $i);
                        $wards->phone = $request->input('ward_phone.' . $i);
                        $wards->fname = $request->input('ward_fname.' . $i);
                        $wards->lname = $request->input('ward_lname.' . $i);
                        $wards->dob = $request->input('dob.' . $i);
                        $wards->create_at = now();
            
                        $wards->save();
    
                        // Assign Wards Data
                        $this->wardsData[$i - 1] = [
                            'fname' => $wards->fname,
                            'lname' => $wards->lname,
                            'email' => $wards->email,
                            'account_type' => 'ward',
                            'parent_email' => $this->parentsData['email'],
                            'password' => substr(str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&'), 0, 8)
                        ];
                    }
                }
            } else {
                $registration->email = $request->email;
                $registration->phone = $request->phone;
                $registration->fname = $request->fname;
                $registration->lname = $request->lname;
                $registration->amount_payable = $request->amount;
                $registration->payment_confirmation = 0;
                $registration->reference = $request->reference;
                $registration->created_at = now();

                $registration->save();

                // Student Registration
                $this->studentsData = [
                    'fname' => $registration->email,
                    'lname' => $registration->lname,
                    'email' => $registration->email,
                    'phone' => $registration->phone,
                    'account_type' => 'student',
                    'password' => substr(str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&'), 0, 8)
                ];
            }

            DB::commit();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            $msg = $e->getMessage();
            $code = $e->getCode();
            $file = $e->getFile();
            $line = $e->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );

            if($request->ajax()) {
                return response()->json([
                    'ERROR' => 'There was an issue saving the data. Please re-enter the data',
                ], 500);
            }
            return redirect()->back()->withErrors(['Error with submission.', 'Please re-enter details.']);

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();

            $msg = $e->getMessage();
            $code = $e->getCode();
            $file = $e->getFile();
            $line = $e->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );

            if($request->ajax()) {
                return response()->json([
                    'ERROR' => 'There was an issue saving the data. Please re-enter the data',
                ], 500);
            }
            return redirect()->back()->withErrors(['Error with submission.', 'Please re-enter details.']);
        }       
       
        try {
            return Paystack::getAuthorizationUrl()->redirectNow();
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            $code = $th->getCode();
            $file = $th->getFile();
            $line = $th->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );

            return redirect()->to('/confirmation')->with('message',"Check your email for payment confirmation. If you don't receive it in 2 hours please send an email to info@metaprogramming.ng or call 234-803-324-9445");
        }
    }

    /**
     * Get feedback from Paystack for receiving payment
     * 
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function handleGatewayCallback()
    {
        // Get paymentData from Paystack
        $paymentDetails = Paystack::getPaymentData();

        if ($paymentDetails['data']['status'] == 'success') {
            $regInfo = Registration::where('reference', $paymentDetails['data']['reference'])->first();
            $regInfo->payment_confirmation = 1;
            $regInfo->save();

            // Once payment has been confirmed, send Payment Notification
            $regInfo->notify(new ConfirmRegisterationPayment($regInfo));

            // Create accounts for confirmed payments
            $register_party = new RegisterController;

            if(isset($this->parentsData)) {
                $register_party->registerParent($this->parentsData, $this->wardsData);
            }
            
            if(isset($this->studentsData)) {
                $register_party->registerStudent($this->studentsData);
            }

            return redirect()->to('/confirmation');
        }
        return redirect()->to('/confirmation')->with('message',"Check your email for payment confirmation. If you don't receive it in 2 hours please send an email to info@metaprogramming.ng or call 234-803-324-9445");
    }
}
