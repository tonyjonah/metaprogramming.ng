<?php

namespace App\Http\Controllers;

use Log;
use Validator;
use Illuminate\Http\Request;
use App\AddEmail;
use App\Notifications\AddEmailNotification;

class AddEmailController extends Controller
{
    public function store(Request $request)
    {
        // Save the message in the db
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($validator->fails()) {

            if($request->ajax()){
                return response()->json("Problem with Entry", 400);
            }

            return redirect()
                    ->withErrors($validator)
                    ->withInput();
        }
        try {
            $add_email = new AddEmail;
            $add_email->email = $request->email;
    
            $add_email->save();
    
            $add_email->notify(new AddEmailNotification($add_email));
        } catch (\Exception $e) {
            return response()->json("Email Exists Already", 400);
        }


        return response()->json(0, 200);
    }
}
