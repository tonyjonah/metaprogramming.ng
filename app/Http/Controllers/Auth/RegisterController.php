<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Events\FreeTrainingRegistration;
use App\Events\PaidAccountCreationEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form. (overloaded)
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        // return view('auth.register');

        setlocale(LC_MONETARY, 'en_NG');

        try {
            $lastId = DB::table('registrations')->latest('id')->first();
        } catch (\Illuminate\Database\QueryException $e) {
            $msg = $e->getMessage();
            $code = $e->getCode();
            $file = $e->getFile();
            $line = $e->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            $code = $th->getCode();
            $file = $th->getFile();
            $line = $th->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );
        }

        $nextId = $lastId->id ?? 0;
        $nextId = str_pad(($nextId + 1), 7, 0, STR_PAD_LEFT);

        return view('registration')->with('regId',$nextId);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname'     => ['required', 'string', 'max:255'],
            'lname'     => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'  => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Get a validator for an incoming registration request for paid attendees.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorForPaidAttendees(array $data)
    {
        return Validator::make($data, [
            'fname'     => ['required', 'string', 'max:255'],
            'lname'     => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone'     => ['required', 'string', 'min:11', 'max:13']
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'fname'     => $data['fname'],
            'lname'     => $data['lname'],
            'email'     => $data['email'],
            'phone'     => $data['phone'],
            'password'  => Hash::make($data['password']),
        ]);
    }

    protected function createForFreeTraining(array $data)
    {
        $cohort_id = DB::table('cohorts')->where('cohort_name', 'Free Tech Training')->get()->first()->id;
        return User::create([
            'fname'     => $data['fname'],
            'lname'     => $data['lname'],
            'email'     => $data['email'],
            'phone'     => '',
            'password'  => Hash::make($data['password']),
            'account_type' => 'student',
            'cohort_id' => $cohort_id,
        ]);
    }

    /**
     * Create a new user instance after a valid registration from a paid Attendee.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function createForPaidAttendees(array $data)
    {
        if(isset($data['parent_email'])) {
            return User::create([
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'parent_email' =>  $data['parent_email'],
                'account_type' => 'ward'
            ]);
        }

        return User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' =>  Hash::make($data['password']),
            'account_type' => ($data['account_type'] == 'student') ? 'student' : 'parent_or_sponsor',
        ]);
    }

    /**
     * Handle a registration request for Parents.
     *
     * @param Array $parentData
     * @param Array $wardsData
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function registerParent(Array $parentData, Array $wardsData = [])
    {
        if(User::where('email', $parentData['email'])->first() != null) {
            $this->validatorForPaidAttendees($parentData)->validate();
            event(new PaidAccountCreationEvent(User::where('email', $parentData['email'])->first()));

            $wards = [];

            if(count($wardsData) == 0) return;
            
            for ($i = 0; $i < count($wardsData); $i++) {
                $this->validatorForPaidAttendees($wardsData[$i])->validate();
                $wards[$i] = $this->createForPaidAttendees($wardsData[$i]);
            }

            Log::info('Notification has been sent to ' . $parentData['email']);
    
            return;
        } else {
            $this->validatorForPaidAttendees($parentData)->validate();

            event(new PaidAccountCreationEvent($user = $this->createForPaidAttendee($parentData)));
    
            $wards = [];
            
            if(count($wardsData) == 0) return;

            for ($i = 0; $i < count($wardsData); $i++) {
                $this->validatorForPaidAttendees($wardsData[$i])->validate();
                $wards[$i] = $this->createForPaidAttendees($wardsData[$i]);
            }

            Log::info('Notification has been sent to ' . $parentData['email']);

            return;
        }
    }

    /**
     * Handle a registration request for Parents.
     *
     * @param Array $studentData
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function registerStudent(Array $studentData)
    {
        $this->validatorForPaidAttendees($studentData)->validate();

        event(new Registered($user = $this->createForPaidAttendee($studentData)));

        Log::info('Notification has been sent to ' . $studentData['email']);

        // $this->guard()->login($user);

        // if ($response = $this->registered($request, $user)) {
        //     return $response;
        // }

        return;
    }

    public function showFreeTraningForm()
    {
        return view('auth.free_training_registeration');
    }

    public function registerFreeTraning()
    {
        $this->validator(request()->all())->validate();

        event(new FreeTrainingRegistration($user = $this->createForFreeTraining(request()->all())));

        $this->guard()->login($user);

        $this->redirectTo = '/faq';

        return redirect($this->redirectPath());
    }

}
