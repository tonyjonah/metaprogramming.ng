<?php

namespace App\Http\Controllers;

use App\Users;
use App\Cohorts;
use App\ProgrammingLectures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller 
{
    /**
     * __contructor()
     * 
     */
    public function __construct()
    {

    }

    /**
     * showDashboard()
     * 
     */
    public function showDashboard(Request $request)
    {
        $user = $request->user();
        return view('student_dashboard');
    }

    /**
     * showSessionInfo()
     * 
     */
    public function showSessionInfo(Request $request)
    {
        $user = $request->user();
        return view('session_info');
    }

    /**
     * showProgrammingLectures()
     */
    public function showProgrammingLectures(Int $id, Request $request)
    {
        $user = $request->user();

        $cohort = Cohorts::find($id);
        if($cohort->class_type == 'programming') {
            $lectures = ProgrammingLectures::all();
            return view('programming-lectures-listing', compact('lectures'));
        }
        return view('student_dashboard');
    }
}