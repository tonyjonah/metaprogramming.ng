<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Illuminate\Http\Request;
use App\Registration;

class RegistrationController extends Controller
{
    /**
     * show()
     * 
     * Shows the registration page
     * 
     * @return \Illuminate\Response\View
     */
    public function show()
    {
        setlocale(LC_MONETARY, 'en_NG');

        try {
            $lastId = DB::table('registrations')->latest('id')->first();
        } catch (\Illuminate\Database\QueryException $e) {
            $msg = $e->getMessage();
            $code = $e->getCode();
            $file = $e->getFile();
            $line = $e->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            $code = $th->getCode();
            $file = $th->getFile();
            $line = $th->getLine();

            Log::error("An Error occured in Class " . __CLASS__);
            Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );
        }

        $nextId = $lastId->id ?? 0;
        $nextId = str_pad(($nextId + 1), 7, 0, STR_PAD_LEFT);

        return view('registration')->with('regId',$nextId);
    }

    /**
     * pay()
     * 
     * Directs those intereted in 
     * registering to paystack
     * for payment fulfilment
     * 
     * @return \Illuminate\Response
     */
    public function pay(Request $request)
    {
        
    }
}
