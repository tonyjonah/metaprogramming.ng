<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ParentSponsor extends Controller
{
    public function showDashboard()
    {
        return view('parent_or_sponsor_dashboard');
    }
}
