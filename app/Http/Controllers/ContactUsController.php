<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Events\ContactMessageCreated;
use Illuminate\Support\Facades\Validator;
use App\Notifications\NotifyOfContactUsMessage;

class ContactUsController extends Controller
{
    public function store(Request $request)
    {
        // Save the message in the db
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required|string',
            'message' => 'required|string',
        ]);

        if($validator->fails()) {
            
            if($request->ajax()){
                return response()->json(0, 400);
            }

            return redirect()
                    ->withErrors($validator)
                    ->withInput();
        }

        $contact_obj = new ContactUs;
        $contact_obj->email = $request->email;
        $contact_obj->name = $request->name;
        $contact_obj->message = $request->message;

        $contact_obj->save();

        $contact_obj->notify(new NotifyOfContactUsMessage($contact_obj));
        Log::notice("New Contact Us Message was sent from " . $contact_obj->email);

        return response()->json(1, 200);
    }

    public function saveAddress(Request $request)
    {
        $contact_obj = new ContactUs;
        $contact_obj->email = $request->email;
        $contact_obj->name = "";
        $contact_obj->message = "";
        $contact_obj->created_at = now();

        $contact_obj->save();

        Log::notice("New Contact email saved " . $contact_obj->email);

        return response()->json(1, 200);
    }
}
