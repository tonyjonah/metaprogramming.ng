<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\PaidAccountCreationEvent;
use App\Events\FreeTrainingRegistration;
use App\Listeners\FreeTrainingAccountRegListener;
use App\Listeners\PaidAccountCreationNotificationListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        PaidAccountCreationEvent::class => [
            PaidAccountCreationNotificationListener::class,
        ],

        FreeTrainingRegistration::class => [
            FreeTrainingAccountRegListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
