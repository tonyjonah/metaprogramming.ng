<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * Table name
     */
    public $table = 'blogs';

    /**
     * Fields which are mass assignable
     */
    public $fillable = [
        'article',
        'title',
        'title_image_url',
        'author',
        'published_at'
    ];
}
