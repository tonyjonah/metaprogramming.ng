<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Registration extends Model
{
    use Notifiable;
    /**
     * 
     */
    public $table = 'registrations';

    /**
     * Indicates if the model should be timestamped.
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable. 
     */
    public $fillable = [
        'email',
        'phone',
        'fname',
        'lname',
        'num_of_wards_registered',
        'feedback',
        'amount_payable',
        'self_registration',
        'payment_confirmation',
        'reference',
        'created_at'
    ];

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array|string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }
}
