<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cohorts extends Model
{
    /**
     * Table name
     */
    protected $table = 'cohorts';

    /**
     * Fields which can be 
     * mass assigned
     */
    protected $fillable = ['cohort_name','class_type','began','ended'];

    /**
     * Hidden fields which can't be
     * mass assigned
     */
    protected $hidden = [];
}