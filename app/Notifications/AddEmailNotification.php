<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AddEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $email_notify;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email_notify)
    {
        $this->email_notify = $email_notify;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Hi,')
                    ->line("We've gotten your email on file " . $this->email_notify->email . " we'll inform you once registeration begins.")
                    ->line("We'll be in touch soon! Take care.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
