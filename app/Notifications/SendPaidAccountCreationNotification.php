<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendPaidAccountCreationNotification extends Notification
{
    use Queueable;

    /**
     * Paid User to be notified with
     * login credentials
     */
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->user->account_type == 'student') return (new MailMessage)
                                                    ->subject('Your MetaProgramming Login Credentials')
                                                    ->greeting('Welcome to MetaProgramming Academy ' . $this->user->fname)
                                                    ->line('Your account has been created. Please find below your username and password')
                                                    ->line('username : ', $this->user->email)
                                                    ->line('password : ', $this->user->password)
                                                    ->action('Notification Action', url('/login'))
                                                    ->line('Thank you for your consideration!');

        if($this->user->account_type == 'parent_or_sponsor') {
            $dateParentOrSponsorRegistered = $this->user->created_at;
            $currentDate = now();

            $mailMessage = new MailMessage;

            // Get the wards that were just registered
            $wards = User::where('parent_email', $this->user->email)->get();
            
            // Check if the time stamp from the instance the date Parent
            // or sponsor was registered is 2 hours more than the ward
            // to know if we need to send credentials now
            if($dateParentOrSponsorRegistered->diffInHours($currentDate) > 2) {
                
                $mailMessage->subject('MetaProgramming Login Credentials');
                $mailMessage->greeting('Thanks for your consideration ' . $this->user->fname);

                if($wards != null) {
                    $mailMessage->line('Here are the credentials of your ward(s) / sponsee(s)');

                    foreach ($wards as $ward) {
                        if($ward->created_at->diffInHours($currentDate) < 2) {
                            $mailMessage->line('For ' . $ward->fname . " " . $ward->lname);
                            $mailMessage->line('username : ' . $ward->email);
                            $mailMessage->line('password : ' . $ward->password);
                        }
                    }

                    $mailMessage->line('Please share these credentials with your ward(s) or those you sponsored');
    
                } else $mailMessage->line('Your sponsee(s) will be notified');

                
                $mailMessage->line('Thank you for your consideration!');

                return $mailMessage;
            }
            else {
                $mailMessage->subject('Your MetaProgramming Login Credentials');
                $mailMessage->greeting('Welcome to MetaProgramming Academy ' . $this->user->fname);
                $mailMessage->line('Your account has been created. Please find below your username and password');
                $mailMessage->line('username : ', $this->user->email);
                $mailMessage->line('password : ', $this->user->password);
                $mailMessage->line('');
                
                if($wards != null) {
                    $mailMessage->line('And here are the credentials of your ward(s) / sponsee(s)');

                    foreach ($wards as $ward) {
                        $mailMessage->line('For ' . $ward->fname . " " . $ward->lname);
                        $mailMessage->line('username : ' . $ward->email);
                        $mailMessage->line('password : ' . $ward->password);
                    }
    
                    $mailMessage->line('Please share these credentials with your ward(s) or those you sponsored');
    
                } else $mailMessage->line('Your sponsee(s) will be notified');
                                
                $mailMessage->action('Notification Action', url('/login'));
                $mailMessage->line('Thank you for your consideration!');

                return $mailMessage;
            }
        }
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
