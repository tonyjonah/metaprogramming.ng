<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyFreeTrainingAttendant extends Notification
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = build_external_url("chat.whatsapp.com", "Ldgwyrx9KxC5ECF8V1WqXv", [], "https");
        return (new MailMessage)
            ->subject('Congrats on Your Successful Registration')
            ->greeting('Welcome to MetaProgramming Academy ' . $this->user->fname)
            ->line('Your free training account has been created.')
            ->line('and you have been logged in.')
            ->line('Please click the link below to ')
            ->line('join the WhatsApp Group for the class')
            ->action('Join WhatsApp Group', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
