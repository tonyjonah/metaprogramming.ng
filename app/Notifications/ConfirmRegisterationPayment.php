<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ConfirmRegisterationPayment extends Notification
{
    use Queueable;

    /**
     * Guardian / Parent who made payment
     */
    public $registrar;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($registrar)
    {
        $this->registrar = $registrar;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Payment Confirmation')
                    ->greeting('Hello ' . $this->registrar->fname . ',')
                    ->line('Your payment has been confirmed. This means your ' . (($this->registrar->num > 1) ? 'children ':'child') . ' will soon begin a life-altering adventure with our Founder, Victor.')
                    ->line('A week before we begin, we will send you an email along with login details.')
                    ->line('Thanks so much for believing in us and keep following us for more updates on Instagram @metaprogramming');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
