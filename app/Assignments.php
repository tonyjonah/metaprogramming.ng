<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignments extends Model
{
    /**
     * Table Name
     */
    public $table = 'assignments';

    public $fillable = [
        'session_id',
        'student_id',
        'content',
        'created_at',
        'updated_at',
    ];
}
