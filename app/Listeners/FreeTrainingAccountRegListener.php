<?php

namespace App\Listeners;

use App\Notifications\NotifyFreeTrainingAttendant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class FreeTrainingAccountRegListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->user->notify((new NotifyFreeTrainingAttendant($event->user)));
    }
}
