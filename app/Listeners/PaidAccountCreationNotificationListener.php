<?php

namespace App\Listeners;

use App\Events\PaidAccountCreationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendAccountCreationNotification
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaidAccountCreationEvent  $event
     * @return void
     */
    public function handle(PaidAccountCreationEvent $event)
    {
        $event->user->notify((new SendAccountCreationNotification($event->user)));
    }
}
