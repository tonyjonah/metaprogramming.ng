<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredWards extends Model
{
    /**
     * 
     */
    public $table = 'registered_wards';

    /**
     * 
     */
    public $timestamps = false;

    /**
     * 
     */
    public $fillable = [
        'registrar_id',
        'email',
        'phone',
        'fname',
        'lname',
        'dob',
        'create_at',
    ];
}
