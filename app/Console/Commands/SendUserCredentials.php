<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\SendCredentials;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendUserCredentials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:credentials {--cohort=} {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to send email credentials for users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(null !== $this->option('cohort')) {
            // Cohort based announcements

            return 0;
        }

        if(null !== $this->option('email')) {
            $user = User::where('email', $this->option('email'))
                        ->first();

            Mail::to($user)->send(new SendCredentials($user, 'H%z8&Jb&'));
    
            $this->info("The email was sent successfully!");
    
            return 0;
        }

    }
}