<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\Mail\SendAnnouncement;
use App\User;

class Announcement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:announcement {--cohort=} {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email annoucement to a cohort';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cohort_id = $this->option('cohort');
        $email = $this->option('email');
        $users = User::where('cohort_id', $cohort_id )
                ->get();

        foreach($users as $user) {
            Mail::to($user)->send(new SendAnnouncement($user, $email));

            $this->info('Email has been sent successfully to ' . $user->fname);
        }

        $this->info('All students have been notified');

        return 0;
    }
}
