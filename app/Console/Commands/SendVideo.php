<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\SendLastWeeksVideo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:video {--cohort=} {--video=} {--videopass=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'We use this command to send videos from the last class to students of the current cohort';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cohort = $this->option('cohort');
        $video = $this->option('video');
        $videopass = $this->option('videopass');

        $users = User::where('cohort_id', $cohort)
                    ->where('account_type', 'student')
                    ->get();

        foreach($users as $user) {
            Mail::to($user)->send(new SendLastWeeksVideo($user, $video, $videopass));

            $this->info('Link to video has been sent successfully to ' . $user->fname . ' !');
        }

        return 0;
    }
}
