<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\SendAssignments;
use Illuminate\Console\Command;

class SendLastClassAssignment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:assignments {--cohort=} {--week=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cohort = $this->option('cohort');
        $week = $this->option('week');

        $users = User::where('cohort_id', $cohort)
                    ->where('account_type', 'student')
                    ->get();

        foreach($users as $user) {
            Mail::to($user)->send(new SendAssignments('Week ' . $week, $user));

            $this->info('Email has been sent successfully to ' . $user->email);
        }

        $this->info('All students have been notified');

        return 0;
    }
}
