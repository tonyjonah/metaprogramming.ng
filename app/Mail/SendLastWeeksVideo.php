<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendLastWeeksVideo extends Mailable
{
    use Queueable, SerializesModels;

    protected $videoLink;
    protected $videoPass;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $videoLink, string $videoPass)
    {
        $this->user = $user;
        $this->videoLink = $videoLink;
        $this->videoPass = $videoPass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@metaprogramming.ng', 'MetaProgramming Academy')
                ->bcc('tony@metaprogramming.ng')
                ->subject("Last Lecture's Recording")
                ->markdown('mails.last_class_video')
                ->with([
                    'fname' => $this->user->fname,
                    'link' => $this->videoLink,
                    'password' => $this->videoPass,
                ]);
    }
}
