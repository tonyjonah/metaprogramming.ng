<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAssignments extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Time period for assignment
     */
    private $week;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(str $week, User $user)
    {
        $this->week = $week;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        // return $this->from('info@metaporgramming', 'MetaProgramming Academy')
        //         ->subject($this->week . 's Assignment')
        //         ->markdown('mails.assignment')
        //         ->with([
        //             'name' => $this->user->fname . ' ' . $this->user->lname,
        //             'book' => '/assignments/' . $this->week . '/'
        //         ]);
    }
}
