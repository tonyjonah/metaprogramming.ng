<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCredentials extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $pass;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->from('info@metaprogramming.ng', 'MetaProgramming Academy')
                ->subject('Your Login Credentials')
                ->markdown('mails.send_credentials')
                ->with([
                        'name' => $this->user->fname . " " . $this->user->lname,
                        'username' => $this->user->email,
                        'password' => $this->pass,
                ]);

    }
}