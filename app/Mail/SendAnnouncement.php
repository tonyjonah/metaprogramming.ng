<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAnnouncement extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $email;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $email)
    {
        $this->user = $user;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@metaprogramming.ng', 'MetaProgramming Academy')
        ->bcc('tony@metaprogramming.ng')
        ->subject("Important Announcement")
        ->markdown('mails.' . $this->email)
        ->with([
            'fname' => $this->user->fname,
            'link' => 'https://chat.whatsapp.com/Ldgwyrx9KxC5ECF8V1WqXv',
        ]);
    }
}
