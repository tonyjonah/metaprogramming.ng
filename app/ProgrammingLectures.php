<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammingLectures extends Model
{
    /**
     * 
     */
    protected $table = 'programming';

    /**
     * 
     */
    protected $fillable = [
        'lecture_name',
        'lecture_info',
        'lecture_slide_path',
    ];
}
