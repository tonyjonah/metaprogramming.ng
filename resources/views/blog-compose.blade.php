@extends ('layouts.main')

@section ('title', 'New Article')
@section('headerStyles')
    @parent
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
@endsection
@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                        <h3 class="uppercase mb0">Single Post</h3>
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                        <div class="feature boxed bg-secondary">
                            <p>
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </p>
                            <form method="post" action="/blog/entry/compose" accept-charset="UTF-8" role="form" name="blogCompose" id="blogCompose" class="text-center">
                                @csrf
                                <input type="text" name="title" class="col-md-8 col-sm-8 col-xs-12" placeholder="Blog Title">
                                <input type="text" name="imageUrl" class="col-md-8 col-sm-8 col-xs-12" placeholder="Blog Title Image">
                                <textarea name="blogEntry" id="blogEntry" cols="80" rows="10" class="col-md-8 col-sm-8 col-xs-12"></textarea>
                                <script>
                                    // Replace the <textarea id="editor1"> with a CKEditor 4
                                    // instance, using default configuration.
                                    CKEDITOR.replace( 'blogEntry' );
                                </script>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    
    @section ('jsScripts')
        @parent
    @endsection
@endsection
