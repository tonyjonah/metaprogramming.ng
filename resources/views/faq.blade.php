@php
    $fmt = new NumberFormatter( 'en_NG', \NumberFormatter::CURRENCY );
@endphp
@extends ('layouts.main')

@section('title', 'FAQ')

@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="mb16">Common questions</h4>
                        <p class="lead mb64">
                            Holla @
                            <a href="mailto:info@metaprogramming.ng">info@metaprogramming.ng</a> if you've got more questions and we'll do our best to answer.
                        </p>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <ul class="accordion accordion-2">
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">What will I learn from the Free Training?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        You will be taught skills people in Tech use in to learn many new tech skills efficiently. You will also learn the value of learning Python and it's uses in the industry today.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">How much does the programme cost?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        The programme is completely free.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Can I Attend in person or remotely?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        You can choose to attend either remotely or in class?.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Where do the physical classes hold?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        The physical classes hold at the Impression Hub which is located at 3, St. Finbarr's College Road, Akoka-Yaba. It's very close to UNILAG.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">What do I need to bring for the classes?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        You would need to bring your Laptop, your notepad and pen to take notes. 
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">How long is the programme?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        The programme will last for 7 days.
                                    </p>
                                </div>
                            </li>
                            <li>
                                @php
                                    $url = build_external_url("chat.whatsapp.com", "Ldgwyrx9KxC5ECF8V1WqXv", [], "https");
                                @endphp
                                <div class="title">
                                    <h4 class="inline-block mb0">How can I get updates about the programme?</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Use this <a href="{{$url}}">link</a> to join the WhatsApp chat for the class.
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <!--end of accordion-->
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    <div class="foundry_modal text-center" data-cookie="welcome-modal" data-time-delay="10000"><i class="ti-close close-modal"></i>
        <h3 class="uppercase">Congratulations on Creating your Free Account</h3>
        <p class="lead mb48">
            Please check your email for the notification sent. Also tell your friends about our programme so we can teach as many people these wonderful skills.
        </p>
        <p>
            We will send out an email once the programme is about to commence with more information. In the mean time, let as many people know about this wonderful opportuninty.
        </p>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection