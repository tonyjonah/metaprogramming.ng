@extends ('layouts.main')

@section('title', 'Dashboard')

@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="mb16">Your Dashboard</h4>
                        <p class="lead mb64">
                            Holla {{Auth::user()->fname}}! <br> 
                            Go through your Lectures or submit your assignments.
                        </p>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-6">
                        <a href="/cohort/2/session/lectures">
                            <div class="feature feature-1">
                                <div class="text-center">
                                    <i class="ti-panel icon"></i>
                                    <h5 class="uppercase">Class Slides</h5>
                                    <p>
                                        Access your lecture notes and accompanying materials.
                                    </p>
                                </div>
                            </div>
                            <!--end of feature-->
                        </a>        
                    </div>
                    <div class="col-sm-6">
                        <a href="">
                            <div class="feature feature-1">
                                <div class="text-center">
                                    <i class="ti-panel icon"></i>
                                    <h5 class="uppercase">Assignments</h5>
                                    <p>
                                        Class Assignments will be accessible from here.
                                    </p>
                                </div>
                            </div>
                            <!--end of feature-->
                        </a>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection