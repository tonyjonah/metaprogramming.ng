@extends ('layouts.main')

@section ('title', 'Contact Us')

@section ('content')
            <section class="page-title page-title-4 bg-secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="uppercase mb0">How to reach us</h3>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="p0">
                <div class="map-holder pt160 pb160">
                    <iframe width="100%" height="320px" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJi1lG9f2MOxARRCzgnosDX_o&key=AIzaSyDp8QLR19CG1KOwpJq9ZUOUOsyyso_j6F8"></iframe>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-5">
                            <h4 class="uppercase">Get In Touch</h4>
                            <p>
                                Would you like to know more about Meta Programming Academy? Feel free to send us a message. We'd love to hear from you.
                            </p>
                            <hr>
                            <p>
                                3, St. Finbarr's College Road
                                <br /> Akoka, Yaba
                                <br /> Lagos
                            </p>
                            <hr>
                            <p>
                                <strong>Email:</strong> info@metaprogramming.ng
                                <br />
                                <strong>Phone:</strong> +234 803 324 9445
                                <br />
                            </p>
                        </div>
                        <div class="col-sm-6 col-md-5 col-md-offset-1">
                            <form method="post" action="/contact" class="form-email" data-success="Thanks for your submission, we will be in touch shortly." data-error="Please fill all fields correctly.">
                                @csrf
                                <input type="text" class="validate-required" name="name" placeholder="Your Name" />
                                <input type="text" class="validate-required validate-email" name="email" placeholder="Email Address" />
                                <textarea class="validate-required" name="message" rows="4" placeholder="Message"></textarea>
                                <button type="submit">Send Message</button>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
    @section ('jsScripts')
        @parent
    @endSection
@endSection