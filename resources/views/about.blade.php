@extends ('layouts.main')

@section('title', 'About')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-2 image-bg overlay parallax">
            <div class="background-image-holder">
                <img alt="Background Image" class="background-image" src="img/girl-in-pink-shirt-holding-notebook.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="uppercase mb8">About Us</h2>
                        <p class="lead mb0">How Do We Plan To Change The Narrative?</p>
                    </div>
                    {{-- <div class="col-md-6 text-right">
                        <ol class="breadcrumb breadcrumb-2">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li class="active">About Us</li>
                        </ol>
                    </div> --}}
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center">
                        <h3 class="mb64 mb-xs-24">We believe everybody has greatness within and we intend to Illuminate this Unique Spark using our programme.</h3>
                        <div class="tabbed-content button-tabs">
                            <ul class="tabs">
                                <li class="active">
                                    <div class="tab-title">
                                        <span>Premise</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            In the world today, depending on how it is perceived, we live in the Information or Knowledge Age.  This world of information is built on data.  The request for data or information is apparent in the business sector.  With regards to the Knowledge, it is shown that how much one knows and carries out mental tasks with acuity, the individual can perform more efficiently in our demanding society for data and information.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Intent</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            We, at Metaprogramming Academy aim to introduce, teach and integrate these two concepts together, Programming and Metacognition, to teenagers and young adults.  We aim to teach techniques in thinking and how to solve concepts creatively and, to instruct students in The Python Programming Language.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Belief</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            With this swiss army knife, the students should be able to take apart tasks easily with much practice.  Not only solving things  in a mathematical way, but rather, approach problems strategically and creatively.  This will put them in a competitive edge for the future.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Outcome</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            With this edge in mind, We aim to produce students who will be ready in disciplines such as Engineering, Science, Research, Technology, Humanities and Business.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!--end of button tabs-->
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
                        <div class="image-tile outer-title text-center">
                            <img alt="Pic" src="img/victor photo.jpeg">
                            <div class="title mb16">
                                <h5 class="uppercase mb0">Victor Ekwueme</h5>
                                <span>Co-Founder</span>
                            </div>
                            <p class="mb0">
                                Victor is the Founding Partner of Meta Programming Academy. He is a partially sighted, self motivated professional who has a wide array of experience in IT ranging from Telecommunications to Data Science. <br>He holds a Bachelor of Science Degree in Computer Science from the University of Lagos, and two (2) Masters Degrees from the University of Notingham and University of Dundee in Information Technology and Intelligent Computational Systems respectively. For more about Victor please follow this <a href="https://www.linkedin.com/in/ogovekwueme/">link</a>.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
                        <div class="image-tile outer-title text-center">
                            <img alt="Pic" src="img/tonys photo.jpg">
                            <div class="title mb16">
                                <h5 class="uppercase mb0">Tony Jonah</h5>
                                <span>Co-Founder</span>
                            </div>
                            <p class="mb0">
                                Tony is an IT Professional with vast experience in IT Infrastructure delivery, managing and deploying solutions in both the Oil & Gas sector and Telecommunications sector in Nigeria. He holds a Bachelor of Science Degree in Computer Engineering from the University of Lagos, as well as a Post Graduate Diploma in IT Project Management from the University of Illinois, Springfield. He is the Founder of the Impression Hub Coworking Space in Akoka-Yaba.
                            </p>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                        <h4 class="uppercase mb16">What will we teach?</h4>
                        <p class="lead mb80">
                            The modules were carefully selected to cater to subjects known for improving Logic, Critical Thinking, Decision Making and Planning.
                        </p>
                    </div>
                </div>
                <!--end of row-->

                <div class="row mb64 mb-x-0">
                    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                        <ul class="lead" data-bullet="ti-check-box">
                            <li>Mindmapping</li>
                            <li>Brainstorming and Mindstorming</li>
                            <li>Flowcharting</li>
                            <li>Mental Arithmetic</li>
                            <li>Memorization Tools</li>
                            <li>Python Programming</li>
                            <li>Problem Solving Tools using topics from the best minds in the world today.</li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection