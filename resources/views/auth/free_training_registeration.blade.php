@extends('layouts.main')

@section('title', 'Free Tech Training')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/grad_photo.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <div class="feature bordered text-center">
                            <h4 class="">Register for the Free Training Here</h4>
                            <form class="text-left" action="{{ route('freetraining') }}" method="post">
                                @csrf
                                <input class="mb4" type="text" name="fname" placeholder="First Name" value="{{ old('fname') }}" required autocomplete="fname" style="width:100%;" autofocus/>

                                @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input class="mb4" type="text" name="lname" placeholder="Last Name" value="{{ old('lname') }}" required autocomplete="lname" style="width:100%;" autofocus/>

                                @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input class="" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" style="width:100%;" autofocus/>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                
                                <input type="password" name="password" placeholder="Password" />
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input type="password" name="password_confirmation" placeholder="Confirm Password" />                                
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <button type="submit">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection        