@extends('layouts.main')

@section('title', 'Email Password')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/grad_photo.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <div class="feature bordered text-center">
                            <h4 class="uppercase">Reset Password</h4>

                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status')}}
                                </div>
                            @endif

                            <form class="text-left" action="{{ route('password.email') }}" method="post">
                                @csrf
                                <input class="" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" style="width:100%;" autofocus/>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input type="submit" value="Send Password Reset Link" />
                            </form>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection