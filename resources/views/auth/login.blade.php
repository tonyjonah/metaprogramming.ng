@extends('layouts.main')

@section('title', 'Login')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/grad_photo.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <div class="feature bordered text-center">
                            <h4 class="uppercase">Login Here</h4>
                            <form class="text-left" action="{{ route('login') }}" method="post">
                                @csrf
                                <input class="" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" style="width:100%;" autofocus/>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input class="mb4" type="password" name="password" placeholder="Password" />

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                
                                <div class="text-center">
                                    <input class="mb4" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="remember" style="color:white;">Remember</label>
                                </div>

                                <input type="submit" value="Login" />
                            </form>
                            <p class="mb0">Forgot your password?
                                <a href="{{ route('password.request') }}">Click Here To Reset</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection        