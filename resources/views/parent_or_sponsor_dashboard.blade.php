@extends ('layouts.main')

@section('title', 'Dashboard')

@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="mb16">Your Dashboard</h4>
                        <p class="lead mb64">
                            Holla {{Auth::user()->fname}}! <br> 
                            You can track the results of your ward(s) or those you sponsored
                        </p>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <ul class="accordion accordion-2">
                            @if (isset($wards))
                                @foreach ($wards as $ward)
                                <li>
                                    <div class="title">
                                        <h4 class="inline-block mb0">{{ $class->title }}</h4>
                                    </div>
                                    <div class="content">
                                        <p>
                                            Meta Programming Academy teaches Python Programming along with Meta Learning skills (Skills which help you learn faster and more efficiently).
                                        </p>
                                    </div>
                                </li>
                                @endforeach
                            @else
                                <div class="content">
                                    <p>
                                        Content coming soon.
                                    </p>
                                </div>
                            @endif
                        </ul>
                        <!--end of accordion-->
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection