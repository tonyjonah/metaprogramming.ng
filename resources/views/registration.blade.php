@php
    $fmt = new NumberFormatter( 'en_NG', \NumberFormatter::CURRENCY );
@endphp
@extends ('layouts.main')

@section('title', 'Register')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-2 image-bg overlay parallax">
            <div class="background-image-holder">
                <img alt="Background Image" class="background-image bg-image-adjust-top-left" src="img/lady on desktop.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="uppercase mb8">Register For The Programme</h2>
                        <p class="lead mb0">Prepare For A Bright Future</p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-10 col-sm-offset-1 col-md-offset-2">
                        <div class="horizontal-tile">
                            <div class="tile-left">
                                <a href="#registerForm">
                                    <div class="background-image-holder fadeIn">
                                        <img src="img/reg1.jpg" alt="MetaProgramming Staff Assisting Students" style="background-size:cover;">
                                    </div>
                                </a>
                            </div>
                            <div class="tile-right bg-secondary">
                                <div class="description">
                                    <h4 class="mb8">Guided Path</h4>
                                    <h6 class="uppercase">
                                        We Will Guide You Along The Way
                                    </h6>
                                    <p>Our Programme is designed to provide you with guidance along the way to ensure the learning experience puts you on your designed learning path.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End of Horizontal Tile -->
                        <div class="horizontal-tile">
                            <div class="tile-left">
                                <a href="#registerForm">
                                    <div class="background-image-holder fadeIn">
                                        <img src="img/reg2.jpg" alt="Graduant Showing Off His Certificate" style="background-size:cover;">
                                    </div>
                                </a>
                            </div>
                            <div class="tile-right bg-secondary">
                                <div class="description">
                                    <h4 class="mb8">Certification</h4>
                                    <h6 class="uppercase">
                                        Confirmation of Competency
                                    </h6>
                                    <p>Our courses provide you with a solid background in Python and Programming. Certificates will be presented at the end of the programme to our diligent students who fulfill the certification requirements.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End of Horizontal Tile -->
                    </div>
                </div>
            </div>
        </section>

        <section id="registerWard">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                <div class="tabbed-content button-tabs text-center">
                    <ul class="tabs">
                        <li class="active">
                            <div class="tab-title">
                                <span>Parent or Sponsor</span>
                            </div>
                        </li>
                        <li class="">
                            <div class="tab-title">
                                <span>Personal Registration</span>
                            </div>
                        </li>
                    </ul>

                    @php
                        $paystackRef = Paystack::genTranxRef();
                    @endphp

                    <ul class="content" id="registerForm">
                        <li class="active">
                            <div class="tab-content">
                                <div class="feature boxed bg-secondary">
                                    <form method="post" action="{{ route('pay') }}" accept-charset="UTF-8" role="form" name="registerWard" class="text-center">
                                        @csrf
                                        <p>
                                            @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </p>
                                        <h4 class="uppercase mt48 mt-xs-0">Provide Us With Some Details</h4>
                                        <p class="lead mb64 mb-xs-24">
                                            To begin this wonderful journey we need some
                                            <br /> information about yourself and your ward(s) or those you are sponsoring.
                
                                        </p>
                                        <p class="mb16 mt16 mb-xs-16 mt-xs-16">
                                            <strong>
                                                <small>
                                                    Please Note that we need at Minimum the First Name, Last Name <br/>
                                                     and Date of Birth of your Child(ren) or Sponsored to create their allocation for the programme.<br/>
                                                    For Sponsors, just indicate the number of people you are sponsoring.<br/>
                                                    It costs {{ $fmt->formatCurrency(40000.00, "NGN") }} per person.
                                                </small>
                                            </strong>
                                        </p>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <div>
                                                <div class="radio-option checked">
                                                    <div class="inner"></div>
                                                    <input type="radio" name="parentOrSponsor" value="parent" checked/>
                                                </div>
                                                <span>Parent</span>
                                            </div>
                                            <div>
                                                <div class="radio-option">
                                                    <div class="inner"></div>
                                                    <input type="radio" name="parentOrSponsor" value="sponsor" />
                                                </div>
                                                <span>Sponsor</span>
                                            </div>
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                1. Your name (Sponsor or Parent's name) ?
                                            </h6>
                                            <input type="text" name="fname" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="First Name">
                                            <input type="text" name="lname" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="Last Name">
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                2. Your contact details ?
                                            </h6>
                                            <input type="email" name="email" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="eMail">
                                            <input type="phone" name="phone" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="Phone Number">
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                3. The number of children / sponsee's being paid for ?
                                            </h6>
                                            <strong><small style="display:block; padding-bottom: 20px;">Please enter the number of Children / Sponsees being paid for to populate the rest of the form.</small></strong>
                                            <input type="number" name="num" id="num_of_wards" class="validate-required field-error">
                                            <hr>
                                        </div>
                                        <div class="overflow-hidden">
                                            <div id="ward_container" class="wardsHidden">
                
                                            </div>
                                        </div>
                                        <div class="overflow-hidden">
                
                                        </div>
                                        <div class="overflow-hidden infoHidden" id="paymentInfo">
                                            <h6 class="uppercase">
                                                4. Make Payment
                                            </h6>
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <p class="text-center">
                                                    For <strong id="wardNum"></strong> you will be paying a total of <strong id="totalCost">N</strong>
                                                </p>
                                                <div class="text-center uppercase">
                                                    Pay Via
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="radio-option checked">
                                                        <div class="inner"></div>
                                                        <input type="radio" name="channels" value="card">
                                                    </div>
                                                    <span>Card</span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="radio-option">
                                                        <div class="inner"></div>
                                                        <input type="radio" name="channels" value="bank_transfer">
                                                    </div>
                                                    <span>Bank Transfer</span>
                                                </div>
                                                <input type="hidden" name="selfRegistration" value="0">
                                                <input type="hidden" name="paymentID" value="{{ $regId }}">
                                                <input type="hidden" name="amount" value=""> {{-- required in kobo --}}
                                                <input type="hidden" name="quantity" value="">
                                                <input type="hidden" name="currency" value="NGN">
                                                {{-- <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > For other necessary things you want to add to your payload. it is optional though --}}
                                                <input type="hidden" name="reference" value="{{ $paystackRef }}"> {{-- required --}}
                                                <button type="submit">Pay</button>
                                            </div>
                
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                        <li class="">
                            <div class="tab-content">
                                <div class="feature boxed bg-secondary">
                                    <form method="post" action="{{ route('pay') }}" accept-charset="UTF-8" role="form" name="registerSelf" id="registerSelf" class="text-center">
                                        @csrf
                                        <p>
                                            @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </p>
                                        <h4 class="uppercase mt48 mt-xs-0">Provide Us With Some Details</h4>
                                        <p class="lead mb64 mb-xs-24">
                                            This form should be filled by those<br/>
                                            paying for themselves to attend the programme.
                
                                        </p>
                                        <p class="mb16 mt16 mb-xs-16 mt-xs-16">
                                            <strong>
                                                <small>
                                                    Late registrations will cost {{ $fmt->formatCurrency(50000.00, "NGN") }}.
                                                </small>
                                            </strong>
                                        </p>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                1. Your name ?
                                            </h6>
                                            <input type="text" name="fname" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="First Name">
                                            <input type="text" name="lname" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="Last Name">
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                2. Your contact details ?
                                            </h6>
                                            <input type="email" name="email" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="eMail">
                                            <input type="phone" name="phone" class="col-md-6 col-sm-6 col-xs-12 validate-required field-error" placeholder="Phone Number">
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <h6 class="uppercase">
                                                    3. Lastly, how did you hear of us?
                                                </h6>
                                                <div class="select-option">
                                                    <i class="ti-angle-down"></i>
                                                    <select name="referrer">
                                                        <option selected value="">Select An Option</option>
                                                        <option value="google">Google</option>
                                                        <option value="website">Our Website</option>
                                                        <option value="friend">A Friend</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="overflow-hidden">
                                            <hr>
                                            <h6 class="uppercase">
                                                4. Make Payment
                                            </h6>
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <p class="text-center">
                                                    The Programme will cost you <strong>{{ $fmt->formatCurrency(40000.00, 'NGN') }}</strong>
                                                </p>
                                                <div class="text-center uppercase">
                                                    Pay Via
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="radio-option checked">
                                                        <div class="inner"></div>
                                                        <input type="radio" name="channels" value="card">
                                                    </div>
                                                    <span>Card</span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="radio-option">
                                                        <div class="inner"></div>
                                                        <input type="radio" name="channels" value="bank_transfer">
                                                    </div>
                                                    <span>Bank Transfer</span>
                                                </div>
                                                <input type="hidden" name="selfRegistration" value="1">
                                                <input type="hidden" name="paymentID" value="{{ $regId }}">
                                                <input type="hidden" name="amount" value=""> {{-- required in kobo --}}
                                                <input type="hidden" name="quantity" value="">
                                                <input type="hidden" name="currency" value="NGN">
                                                {{-- <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > For other necessary things you want to add to your payload. it is optional though --}}
                                                <input type="hidden" name="reference" value="{{ $paystackRef }}"> {{-- required --}}
                                                <button type="submit">Pay</button>
                                            </div>
                
                                        </div>
                                    </form>                                    
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    @section ('jsScripts')
        @parent
    @endsection
@endsection