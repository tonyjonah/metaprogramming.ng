@extends ('layouts.main')

@section('title', 'Session Info')

@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="uppercase mb80">
                            Your Current Session Information
                        </h4>
                        <div class="tabbed-content button-tabs vertical">
                            <ul class="tabs">
                                <li class="active">
                                    <div class="tab-title">
                                        <span>Zoom Information</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Announcements</span>
                                    </div>
                                </li>
                            </ul>
                            <ul class="content">
                                <li class="active">
                                    <div class="tab-content">
                                        <h5 class="uppercase">Google Meeting Info</h5>
                                        <hr>
                                        <p>
                                            <a href="https://meet.google.com/uti-kcnu-msu">https://meet.google.com/uti-kcnu-msu</a>
                                        </p>
                                    </div>
                                </li>
                                {{-- <li class="">
                                    <div class="tab-content">
                                        <h5 class="uppercase">Cohort 3's Mid-Session Activity</h5>
                                        <hr>
                                        <p>
                                            Our mid-week session this week will take place on Thursday the 14th of January due to some logistic challenges. <br> We apologise for any inconvenience this may cause.
                                        </p>
                                    </div>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection    