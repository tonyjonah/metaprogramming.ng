<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('title', 'Home Page') | Meta Programming Academy</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        @section('headerStyles')
            <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{asset('css/themify-icons.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{asset('css/flexslider.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{asset('css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{asset('css/ytplayer.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{asset('css/theme-chipotle.css')}}" rel="stylesheet" type="text/css" media="all" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        @show
        <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    </head>
    <body class="scroll-assist btn-rounded">
        <div class="nav-container">
            <a id="top"></a>
            <nav>
                <div class="nav-bar">
                    <div class="module left">
                        <a href="/">
                            <img class="logo logo-light" alt="MetaProgramming" src="{{asset('img/logo-light.png')}}" />
                            <img class="logo logo-dark" alt="MetaProgramming" src="{{asset('img/logo-light.png')}}" />
                        </a>
                    </div>
                    <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="module-group right">
                        <div class="module left">
                            <ul class="menu">
                                <li class="">
                                    <a href="/">
                                        Home
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/about">
                                        About Us
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/register">
                                        Registration
                                    </a>
                                </li>
                                <li class="has-dropdown">
                                    <a href="#">
                                        Classes
                                    </a>
                                    <ul>
                                        <li class="">
                                            <a href="/faq">
                                                FAQ
                                            </a>
                                        </li>
                                        @if(Auth::user())
                                        <li class="">
                                            <a href="/student/dashboard">
                                                My Dashboard
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="/student/information">
                                                Session Information
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="/logout">
                                                Logout
                                            </a>
                                        </li>
                                        @else
                                        <li class="">
                                            <a href="/login">
                                                Student Login
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="/blog/entries">
                                        Blog
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/contact">
                                        Contact Us
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of module group-->
                </div>
            </nav>
        </div>
        <div class="main-container">
            @yield('content')
            <footer class="footer-1 bg-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="sub">&copy; Copyright 2019 - Meta Programming Academy</span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <ul class="list-inline social-list">
                                <li>
                                    <a href="https://twitter.com/MetaProgAcademy">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/MetaProgramming">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/metaprogramming/?hl=en">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--end of container-->
                <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
            </footer>
        </div>
        @section('jsScripts')
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/flickr.js')}}"></script>
        <script src="{{asset('js/flexslider.min.js')}}"></script>
        <script src="{{asset('js/lightbox.min.js')}}"></script>
        <script src="{{asset('js/masonry.min.js')}}"></script>
        {{-- <script src="{{asset('js/twitterfetcher.min.js')}}"></script> --}}
        <script src="{{asset('js/spectragram.min.js')}}"></script>
        <script src="{{asset('js/ytplayer.min.js')}}"></script>
        <script src="{{asset('js/countdown.min.js')}}"></script>
        <script src="{{asset('js/smooth-scroll.min.js')}}"></script>
        <script src="{{asset('js/parallax.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        @show
    </body>
</html>