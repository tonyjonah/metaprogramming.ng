@component('mail::message')
Hello **{{ $name }}**,


Your login credentials are below
@component('mail::panel')
username is {{ $username }}  
password is {{ $password }}  
@endcomponent


Thanks,
{{ config('app.name') }}
@endcomponent