@component('mail::message')
Hello **{{ $fname ?? '' }}**,


Review the video from the last class  
@component('mail::panel')
Here's the link for the video of the last class {{ $link ?? '' }}  
Use the password to access the video {{ $password ?? '' }}  
@endcomponent


Enjoy,  
{{ config('app.name') }}
@endcomponent