@component('mail::message')
Hello **{{ $fname ?? '' }}**,


Join WhatsApp Chat 
@component('mail::panel')
Please use this link to join our WhatsApp chat for the session, if you've not already done so {{ $link ?? '' }}
This will be our main communication channel for passing information across during the session. 
@endcomponent

Session Schedule Change 
@component('mail::panel')
Due to some unforseen circumstances on our part, we will be shifting the start date of the free training series from Monday 21st to Monday the 28th.
This change is to allow one of our trainers attend to a critical business need which came up suddenly. However, we will still be delivering an introductory session to all students registered by 11:00 AM on the Monday, the 21st / Today. Please join the WhatsApp channel to get the link for the session.
Also feel free to pass the registration link along to your friends.
@endcomponent

See You in Class Today,  
{{ config('app.name') }}
@endcomponent