@extends ('layouts.main')

@section('title', 'Articles')

@section ('content')
    <div class="main-container">
        <section>
            <div class="container">
                @foreach ($blogs as $blog)
                <div class="feed-item mb96 mb-xs-48">
                    <div class="row mb16 mb-xs-0">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                            {{-- <h6 class="uppercase mb16 mb-xs-8">{{$blog['created_at']}}</h6> --}}
                            <h3>{{ $blog['title'] }}</h3>
                            <h6>{{ $blog['author']}}</h6>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row mb32 mb-xs-16">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                            {{-- <img alt="Article Image" class="mb32 mb-xs-16" src="{{asset('img/blog-single.jpg')}}" /> --}}
                            {!! $blog['article'] !!}
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                            <a class="mb48 mb-xs-32 btn btn-lg" href="#">Share Article</a>
                            <hr>
                        </div>
                    </div>
                </div>
                <!--end of feed item-->
                @endforeach

                
                {{-- <div class="feed-item mb96">
                    <div class="row mb16 mb-xs-0">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                            <h6 class="uppercase mb16 mb-xs-8">September 4th</h6>
                            <h3>You can still embed media types
                                <br /> using this blog type.</h3>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row mb32 mb-xs-16">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                            <div class="embed-video-container embed-responsive embed-responsive-16by9 mb32 mb-xs-16">
                                <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/25737856?badge=0&title=0&byline=0&title=0" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                            <!--end of embed video container-->
                            <p class="lead">
                                Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
                            </p>
                            <p class="lead">
                                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                            <a class="mb48 mb-xs-32 btn btn-lg" href="#">Share Article</a>
                            <hr>
                        </div>
                    </div>
                </div> --}}
                <!--end of feed item-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section ('jsScripts')
        @parent
    @endsection
@endsection