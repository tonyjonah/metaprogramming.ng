@extends ('layouts.main')

@section('title', 'Python Classes')

@section ('content')
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="mb80 uppercase">Programming Classes</h4>
                    <div class="tabbed-content button-tabs vertical">
                        <ul class="tabs">
                            @foreach ($lectures as $lecture)
                                @if($loop->first)
                                <li class="active">
                                    <div class="tab-title">
                                        <span>Class {{$loop->index + 1}}</span>
                                    </div>
                                </li>
                                @else
                                    <li>
                                        <div class="tab-title">
                                            <span>Class {{$loop->index + 1}}</span>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <ul class="content">
                            @foreach ($lectures as $lecture)
                                @if($loop->first)
                                    <li class="active">
                                        <div class="tab-content">
                                            <h5 class="uppercase">{{ $lecture->lecture_name }}</h5>
                                            <hr>
                                            <p>
                                                {{ $lecture->lecture_info }}
                                            </p>
                                            <a href="{{ '/' . $lecture->lecture_slide_path }}">
                                                <i class="ti-file"></i>
                                            </a>
                                        </div>
                                    </li>
                                @else
                                    <li class="">
                                        <div class="tab-content">
                                            <h5 class="uppercase">{{ $lecture->lecture_name }}</h5>
                                            <hr>
                                            <p>
                                                {{ $lecture->lecture_info }}
                                            </p>
                                            <a href="{{ '/' . $lecture->lecture_slide_path }}">
                                                <i class="ti-file"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endif


                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection
