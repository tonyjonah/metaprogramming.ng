@extends ('layouts.main')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-4 image-bg background-multiply overlay parallax">
            <div class="background-image-holder bg-image-adjust-left">
                <img alt="Background Image" class="background-image" src="img/receptionist.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="uppercase mb0">Services</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <ol class="breadcrumb breadcrumb-2">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li class="active">Services</li>
                        </ol>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="feature feature-1 boxed">
                            <div class="text-center">
                                <i class="ti-pulse icon"></i>
                                <h4>Elegant and usable templates,
                                    <br class="hidden-sm" /> crafted with purpose.</h4>
                            </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
                            </p>
                        </div>
                        <!--end of feature-->
                    </div>
                    <div class="col-sm-6">
                        <div class="feature feature-1 boxed">
                            <div class="text-center">
                                <i class="ti-dashboard icon"></i>
                                <h4>Built from the ground up
                                    <br class="hidden-sm" /> for speed and performance.</h4>
                            </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
                            </p>
                        </div>
                        <!--end of feature-->
                    </div>
                    <div class="col-sm-6">
                        <div class="feature feature-1 boxed">
                            <div class="text-center">
                                <i class="ti-layers icon"></i>
                                <h4>A design system that includes
                                    <br class="hidden-sm" /> a massive array of elements.</h4>
                            </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
                            </p>
                        </div>
                        <!--end of feature-->
                    </div>
                    <div class="col-sm-6">
                        <div class="feature feature-1 boxed">
                            <div class="text-center">
                                <i class="ti-package icon"></i>
                                <h4>An all-inclusive package
                                    <br class="hidden-sm" /> complete with intuitive page builder.</h4>
                            </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
                            </p>
                        </div>
                        <!--end of feature-->
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="pt64 pb64">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="mb8">Create something beautiful.</h2>
                        <p class="lead mb40">
                            Variant Page Builder, Over 100 Page Templates - The choice is clear.
                        </p>
                        <a class="btn btn-filled btn-lg mb0" href="#">Purchase Foundry</a>
                    </div>
                </div>
                <!--end of row-->
                <div class="embelish-icons">
                    <i class="ti-marker"></i>
                    <i class="ti-layout"></i>
                    <i class="ti-ruler-alt-2"></i>
                    <i class="ti-eye"></i>
                    <i class="ti-signal"></i>
                    <i class="ti-pulse"></i>
                    <i class="ti-marker"></i>
                    <i class="ti-layout"></i>
                    <i class="ti-ruler-alt-2"></i>
                    <i class="ti-eye"></i>
                    <i class="ti-signal"></i>
                    <i class="ti-pulse"></i>
                </div>
            </div>
            <!--end of container-->
        </section>
        <footer class="footer-1 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <img alt="Logo" class="logo" src="img/logo-light.png" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Recent Posts</h6>
                            <hr>
                            <ul class="link-list recent-posts">
                                <li>
                                    <a href="#">Hugging pugs is super trendy</a>
                                    <span class="date">February
                                        <span class="number">14, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Spinning vinyl is oh so cool</a>
                                    <span class="date">February
                                        <span class="number">9, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Superior theme design by pros</a>
                                    <span class="date">January
                                        <span class="number">27, 2015</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Latest Updates</h6>
                            <hr>
                            <div class="twitter-feed">
                                <div class="tweets-feed" data-feed-name="hub_impression">
                                </div>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Instagram</h6>
                            <hr>
                            <div class="instafeed" data-user-name="d_impressionhub">
                                <ul></ul>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-6">
                        <span class="sub">&copy; Copyright 2017 - Impression Hub</span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <ul class="list-inline social-list">
                            <li>
                                <a href="#">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-vimeo-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--end of container-->
            <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
        </footer>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection