@extends('layouts.main')

@section ('content')
{{-- <section class="pt0 pb0">
	<div class="slider-all-controls">
		<ul class="slides">
			<li class="vid-bg image-bg overlay pt240 pb240 flex-active-slide">
				<div class="background-image-holder fadeIn">
					<img src="img/grad_photo.jpg" alt="Background Image" class="background-image">
				</div>
				<div class="fs-vid-background">
					<video muted loop>
						<source src="video/class1_trim1.webm" type="video/webm">
						<source src="video/class1_trim1.mp4" type="video/mp4">
						<source src="video/class1_trim1.ogv" type="video/ogg">
					</video>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<div class="col-sm-12 text-center">
								<strong><div class="countdown mb40" data-date="2021/6/26" style="font-weight: 500;"></div></strong>
							</div>
							<h4 class="large uppercase" style="font-weight: 600;">Our next session begins on the 26th of June.</h4>
							<p class="lead">
								Classes begin on the 26th of June. The session will last 6 classes for 3 Hours each.
							</p>
							<p>
								We are still accepting registration requests. Visit <strong><a href="{{ url('/registration') }}">https://metaprogramming.ng/registration</a></strong>.<br> To find out more send an email to <a href="mailto:info@metaprogramming.ng">info@metaprogramming.ng</a>.
							</p>
						</div>
					</div>
					<!--end of row-->
				</div>
			</li>
			<li class="vid-bg image-bg overlay pt240 pb240">
				<div class="background-image-holder fadeIn">
					<img src="img/IMG_2371.jpg" alt="Background Image" class="background-image">
				</div>
				<div class="fs-vid-background">
					<video muted loop>
						<source src="video/lecture2_trim.webm" type="video/webm">
						<source src="video/lecture2_trim.mp4" type="video/mp4">
						<source src="video/lecture2_trim.ogv" type="video/ogg">
					</video>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<div class="col-sm-12 text-center">
								<strong><div class="countdown mb40" data-date="2021/6/26" style="font-weight: 500;"></div></strong>
							</div>
							<h4 class="large uppercase" style="font-weight: 600;">Our next session begins on the 26th of June.</h4>
							<p class="lead">
								Classes begin on the 26th of June. The session will last 6 classes for 3 Hours each.
							</p>
							<p>
								We are still accepting registration requests. Visit <strong><a href="{{ url('/registration') }}">https://metaprogramming.ng/registration</a></strong>.<br> To find out more send an email to <a href="mailto:info@metaprogramming.ng">info@metaprogramming.ng</a>.
							</p>
						</div>
					</div>
					<!--end of row-->
				</div>
			</li>
			<li class="vid-bg image-bg overlay pt240 pb240">
				<div class="background-image-holder fadeIn">
					<img src="img/IMG_2371.jpg" alt="Background Image" class="background-image">
				</div>
				<div class="fs-vid-background">
					<video muted loop>
						<source src="video/Last Class Trim.webm" type="video/webm">
						<source src="video/Last Class Trim.mp4" type="video/mp4">
						<source src="video/Last Class Trim.ogv" type="video/ogg">
					</video>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<div class="col-sm-12 text-center">
								<strong><div class="countdown mb40" data-date="2021/6/26" style="font-weight: 500;"></div></strong>
							</div>
							<h4 class="large uppercase" style="font-weight: 600;">Our next session begins on the 26th of June.</h4>
							<p class="lead">
								Classes begin on the 26th of June. The session will last 6 classes for 3 Hours each.
							</p>
							<p>
								We are still accepting registration requests. Visit <strong><a href="{{ url('/registration') }}">https://metaprogramming.ng/registration</a></strong>.<br> To find out more send an email to <a href="mailto:info@metaprogramming.ng">info@metaprogramming.ng</a>.
							</p>
						</div>
					</div>
					<!--end of row-->
				</div>
			</li>
			<li class="vid-bg image-bg overlay pt240 pb240">
				<div class="background-image-holder fadeIn">
					<img src="img/IMG_2371.jpg" alt="Background Image" class="background-image">
				</div>
				<div class="fs-vid-background">
					<video muted loop>
						<source src="video/Interview With Annie Trim.webm" type="video/webm">
						<source src="video/Interview With Annie Trim.mp4" type="video/mp4">
						<source src="video/Interview With Annie Trim.ogv" type="video/ogg">
					</video>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<div class="col-sm-12 text-center">
								<strong><div class="countdown mb40" data-date="2021/6/26" style="font-weight: 500;"></div></strong>
							</div>
							<h4 class="large uppercase" style="font-weight: 600;">Our next session begins on the 26th of June.</h4>
							<p class="lead">
								Classes begin on the 26th of June. The session will last 6 classes for 3 Hours each.
							</p>
							<p>
								We are still accepting registration requests. Visit <strong><a href="{{ url('/registration') }}">https://metaprogramming.ng/registration</a></strong>.<br> To find out more send an email to <a href="mailto:info@metaprogramming.ng">info@metaprogramming.ng</a>.
							</p>
						</div>
					</div>
					<!--end of row-->
				</div>
			</li>
		</ul>
	</div>
</section> --}}

<section class="pt120 pb120 image-bg">
	<div class="background-image-holder free-tech-training fadeIn">
		<img class="background-image" src="img/free_training_banner.jpg" alt="Background" style="display:none;">
	</div>

	<div class="container">
		<div class="row">
			<div class="col-sm-12 mb-xs-80">
				<h3 class="mb16"> 
					<br class="hidden-sm"> </h3>
				<h6 class="uppercase mb32">  
					<br class="hidden-sm"> </h6>
				<a class="btn btn-filled btn-lg mb0" href="/freetechtraining">Register</a>
			</div>
		</div>
	</div>
</section>

<section class="fullscreen parallax">
	<div class="container v-align-transform">
		<div class="row">
			<div class="col-sm-12 text-center">
				<img class="image-small mt16 mt-xs-120 mb80" src="img/logo-light.png" alt="Logo">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<h2 class="uppercase large text-center" style="text-shadow:1px 1px 5px #aaa"><strong>Creating</strong></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 text-center uppercase"><h4><strong>Smarter</strong></h4></div>
			<div class="col-sm-4 text-center uppercase"><h4 class="color-primary"><strong>&#9666; Faster  &#9656;</strong></h4></div>
			<div class="col-sm-4 text-center uppercase"><h4><strong>Better</strong></h4></div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 text-center">
				<h6 class="uppercase mt16">Leaders for the New Age</h6>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 text-center mb120">
				<strong>If you can learn, You can earn.<br class="hidden-sm"></strong> Hop on board to learn effective, age old techniques that will cut learning time in half and place your children and yourselves on the path to financial success.
			</div>
		</div>
	</div>
</section>

<section class="image-square left">
	<div class="col-md-6 image">
		<div class="background-image-holder fadeIn">
			<img class="background-image" src="img/development.jpg" alt="">
		</div>
	</div>

	<div class="col-md-6 col-md-offset-1 content">
		<div class="feature feature-2 bordered" style="border-radius:10px;box-shadow:2px 1px 8px #999;">
			<div class="text-center">
				<i class="fas fa-laptop-code" style="font-size:3em;"></i>
				<h4 class="uppercase color-primary">Python Programming</h4>
			</div>
			<p class="text-center">
				Our Python Programming Course takes our students through the fundamentals of programming, from concepts such as Psuedocode and Data Structures, to advanced topics such as Object Oriented Programming using the Python Programming Language.
			</p>
			<p class="text-center">
				Giving them the foundation to delve into Professional Fields like Data Science and Web Development.
			</p>
			<div class="text-center">
				<a class="btn btn-filled btn-lg mb0" href="/about">Learn More</a>
			</div>
		</div>
	</div>
</section>

<section class="image-square right">
	<div class="col-md-6 image">
		<div class="background-image-holder fadeIn">
			<img src="img/learning scrabble.jpg" alt="Scabble Pieces Learning" class="background-image" style="display:none;">
		</div>
	</div>

	<div class="col-md-6 content">
		<div class="feature feature-2 bordered" style="border-radius:10px;box-shadow:2px 1px 8px #999;">
			<div class="text-center">
				<i class="fas fa-graduation-cap" style="font-size:3em"></i>
				<h4 class="uppercase color-primary">Meta Learning</h4>
			</div>
			<p class="text-center">
				The ability to Learn new skills effeciently is a requirement that high paying careers demand. Your ability to learn will determine by far your adaptability to the changes we will be seeing in the coming years. Give your children and yourselves an edge by attending our Meta Learning Course where we will be breaking down myths about the brains capacity to absorb new information and teaching age old techniques that will improve your memorization, enhance your ability to plan, provide you with effective thinking abilities and improve your mental math skills.
			</p>
			<div class="text-center">
				<a class="btn btn-filled btn-lg mb0" href="/about">Learn More</a>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
				<h4 class="uppercase mb16">
					And More Courses Underway
				</h4>
				<p class="lead mb40">We've seen the demand and will be releasing dates for the roll-out of our new courses.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-10 col-sm-offset-1 col-md-offset-2">
				<div class="horizontal-tile">
					<div class="tile-left">
						<a href="#">
							<div class="background-image-holder fadeIn" style="background-color: rgba(0, 0, 0, 0); background-position: initial; background-repeat: repeat; background-attachment: scroll; background-image: url(&quot;img/project-single-1.jpg&quot;); background-size: auto; background-origin: padding-box; background-clip: border-box;">
								<img alt="image" class="background-image" src="img/jscode.jpg" style="display: none;">
							</div>
						</a>
					</div>
					<div class="tile-right bg-secondary">
						<div class="description">
							<h4 class="mb8">Web Development</h4>
							<h6 class="uppercase">
								Python Web Development with Django
							</h6>
							<p>
								The course will be teaching Web Development using the Python Django Framework.
							</p>
						</div>
					</div>
				</div>
				<!--end of horizontal tile-->
				<div class="horizontal-tile">
					<div class="tile-left">
						<a href="#">
							<div class="background-image-holder fadeIn" style="background-color: rgba(0, 0, 0, 0); background-position: initial; background-repeat: repeat; background-attachment: scroll; background-image: url(&quot;img/project-single-3.jpg&quot;); background-size: auto; background-origin: padding-box; background-clip: border-box;">
								<img alt="image" class="background-image" src="img/focus planning.jpg" style="display: none;">
							</div>
						</a>
					</div>
					<div class="tile-right bg-secondary">
						<div class="description">
							<h4 class="mb8">Data Science</h4>
							<h6 class="uppercase">
								Python Data Science
							</h6>
							<p>
								The course will be preparing students for roles where insights into Data are required to create rich experiences.
							</p>
						</div>
					</div>
				</div>
				<!--end of horizontal tile-->
			</div>
		</div>
	</div>
</section>

<div class="foundry_modal text-center" data-cookie="newsletter-signup-modal" data-time-delay="10000"><i class="ti-close close-modal"></i>
	<h3 class="uppercase">Want To Get Updated?</h3>
	<p class="lead mb48">
		Stay in the loop for new courses and price changes. We'll send you 
		<br> updates of our latest courses.
	</p>
	<form method="POST" action="/contact/address" name="contactDetails" class="form-newsletter halves" data-success="Thanks, we will be in touch.">
		@csrf
		<input type="text" name="email" class="mb0 validate-email validate-required  signup-email-field" placeholder="Email Address">
		<button type="submit" name="submitBtn" class="btn-white mb0">Keep Me Informed</button>
	</form>
</div>
	@section ('jsScripts')
		@parent
	@endSection
@endsection